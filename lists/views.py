from django.http import HttpResponse
from django.shortcuts import redirect, render
from lists.models import Item, List
import random

# # Create your views here.
def home_page(request):
    return render(request, 'home.html')
def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    comment = tambahComment(list_)
    return render(request, 'list.html', {'list': list_, 'comment': comment})

def new_list(request):
    list_ = List.objects.create()
    n = random.randint(1,30)
    text=request.POST['item_text']
    x = int(text)
    item = Item()
    item.angka = x
    item.angkaacak = n
    item.statusnya = status(x, n)
    item.list = list_
    item.save()
    return redirect('/lists/%d/' % (list_.id,))

def add_item(request, list_id):
    list_ = List.objects.get(id=list_id)
    n = random.randint(1,30)
    text=request.POST['item_text']
    x = int(text)
    item = Item()
    item.angka = x
    item.angkaacak = n
    item.statusnya = status(x, n)
    item.list = list_
    item.save()
    return redirect('/lists/%d/' % (list_.id,))

def status(x, y):
	if x < y+3 and x > y-3:
		return 'Kamu menang!!'
	else:
		return 'Kamu kalah!!'

def tambahComment(list_id):
    list_temp = Item.objects.filter(list=list_id).reverse()
    counter = 0
    comment = ''

    if len(list_temp) < 3:
        return comment

    for i in range(3):
        item=list_temp[i]
        if item.statusnya == 'Kamu menang!!':
            counter = counter+1

    if counter==3:
        comment='Ulala!!'

    if len(list_temp) < 5:
        return comment

    counter = 0

    for i in range(5):
        item=list_temp[i]
        if item.statusnya == 'Kamu menang!!':
            counter = counter+1

    if counter==5:
        comment='Ulalala'

    if len(list_temp) < 10:
        return comment

    counter = 0

    for i in range(10):
        item=list_temp[i]
        if item.statusnya == 'Kamu menang!!':
            counter = counter+1

    if counter==10:
        comment='Uulala'

    return comment