# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0005_auto_20151217_1306'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='status',
            new_name='statusnya',
        ),
    ]
