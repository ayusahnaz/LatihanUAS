from django.template.loader import render_to_string
from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest

from lists.models import Item, List
from lists.views import home_page

# a. Record permainan dapat disimpan ke dalam database
# b. Record permainan dapat ditampilkan ke halaman web
# c. Setiap pemain memiliki alamat URL yang unik setelah melakukan permainan pertama kalinya
# d. Menyimpan dan menampilkan record permainan kedua, ketiga, dst
# e. Menampilkan record permainan untuk alamat URL spesifik

#kelas tes yang terkait dengan home page
class HomePageTest(TestCase):
    pass
#kelas tes yang terkait dengan kedua list dan item
class ListAndItemModelsTest(TestCase):
    def test_no_win(self):
        a_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)

        response = self.client.get('/lists/%d/' % (a_list.id,))
        self.assertNotContains(response, 'Ulala!!')
        self.assertNotContains(response, 'Ulalala')
        self.assertNotContains(response, 'Uulala')

    def test_win_three(self):
        a_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)

        response = self.client.get('/lists/%d/' % (a_list.id,))
        self.assertContains(response, 'Ulala!!')
        self.assertNotContains(response, 'Ulalala')
        self.assertNotContains(response, 'Uulala')

    def test_win_five(self):
        a_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)

        response = self.client.get('/lists/%d/' % (a_list.id,))
        self.assertNotContains(response, 'Ulala!!')
        self.assertContains(response, 'Ulalala')
        self.assertNotContains(response, 'Uulala')

    def test_win_ten(self):
        a_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!', list=a_list)

        response = self.client.get('/lists/%d/' % (a_list.id,))
        self.assertNotContains(response, 'Ulala!!')
        self.assertNotContains(response, 'Ulalala')
        self.assertContains(response, 'Uulala')

class ListViewTest(TestCase):
    # nomor b
    def test_displays_items_to_web(self):
        correct_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!',list=correct_list)
        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertContains(response, 'Kamu menang!!')
    
    # nomor e
    def test_displays_only_items_for_that_list(self):
        correct_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!',list=correct_list)
        
        other_list = List.objects.create()
        Item.objects.create(angkaacak=15, angka=10, statusnya='Kamu kalah...',list=other_list)

        response = self.client.get('/lists/%d/' % (correct_list.id,))
        response2 = self.client.get('/lists/%d/' % (other_list.id,))

        self.assertContains(response, 'Kamu menang!!')
        self.assertNotContains(response, 'Kamu kalah...')
    
    # nomor c
    def test_passes_correct_list_to_template(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertEqual(response.context['list'], correct_list)

class NewListTest(TestCase):
    # nomor a
    def test_record_game_to_database(self):
        a_list = List.objects.create()
        Item.objects.create(angkaacak=10, angka=9, statusnya='Kamu menang!!', list=a_list)

        self.assertEqual(Item.objects.count(), 1)

class NewItemTest(TestCase):
    # nomor d
    def test_record_second_third_etc_game(self):
        list_nya = List.objects.create()
        Item.objects.create(angkaacak=15, angka=15, statusnya='Kamu menang!!',list=list_nya)
        a = self.client.post(
            '/lists/%d/add_item' % (list_nya.id,),
            data={'item_text': '17'}
        )
        response = self.client.get('/lists/%d/' % (list_nya.id))
        self.assertEqual(Item.objects.count(),2)
        self.assertContains(response, 'Tebakan: 15')
        self.assertContains(response, 'Tebakan: 17')
        #self.fail(response)